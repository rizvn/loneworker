//
//  LoginViewController.swift
//  LoneWorker
//
//  Created by Riz on 24/10/2015.
//  Copyright © 2015 Riz. All rights reserved.
//

import UIKit


class LoginViewController : UIViewController{

    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        txtPass.text = ""
        txtUser.text = ""
        
        let username = NSUserDefaults.standardUserDefaults().stringForKey(Utils.Keys.USERNAME.rawValue)
        
        if(username != nil){
            displayTimerView()
        }
    }
    
    func displayTimerView(){
        self.performSegueWithIdentifier("timerView", sender: self)
    }
    
    
    @IBAction func loginButtonTouched(sender: AnyObject) {
       let user = txtUser.text!
       let pass = txtPass.text!
        
       if(login(user, pass)){
          displayTimerView()
       }
    }
    
    func displayAlert(message: String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(okAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    func login(username:String, _ pass: String)-> Bool{
        
        //validate input
        if(username.isEmpty || pass.isEmpty){
            displayAlert("Invalid username or password")
            return false
        }
        
        //send request
        
        
        
        //validte response
        
        
        
        
        //store result
        NSUserDefaults.standardUserDefaults().setObject("riz", forKey: Utils.Keys.USERNAME.rawValue)
        
        
       
        
        //return
        return true
    }
}
