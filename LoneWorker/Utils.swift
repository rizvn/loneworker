//
//  Utils.swift
//  LoneWorker
//
//  Created by Riz on 24/10/2015.
//  Copyright © 2015 Riz. All rights reserved.
//

import Foundation

class Utils {
   
    
    enum Keys : String{
        case USERNAME = "USERNAME"
        case PASSWORD = "PASSWORD"
        case AUTH_TOKEN = "AUTH_TOKEN"
    }
    
}