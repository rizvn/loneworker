//
//  ViewController.swift
//  LoneWorker
//
//  Created by Riz
//  Copyright © 2015 Riz. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate{
  
  @IBOutlet weak var timerLabel: UILabel!
  @IBOutlet weak var startButton: UIButton!
  @IBOutlet weak var stopButton: UIButton!
  @IBOutlet weak var progressBar: MBCircularProgressBarView!
  @IBOutlet weak var minus5Button: UIButton!
  @IBOutlet weak var plus5Button: UIButton!
  
  var secondsRemaining:Int = 0
  var startSeconds:Int = 0
  var timer : NSTimer!
  let locationManager = CLLocationManager()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    initLocationManager()

  }
  
  func initLocationManager(){
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    self.locationManager.requestAlwaysAuthorization() //this only works in ios8+ might need to change
    self.locationManager.startUpdatingLocation()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  @IBAction func logoutTouched(sender: UIButton) {
    clearUserStorage()
    self.navigationController?.popViewControllerAnimated(true)
  }
  
  @IBAction func startTimer(sender: UIButton){
    startButton.hidden = true
    stopButton.hidden = false
    plus5Button.hidden = true
    minus5Button.hidden = true
    startSeconds = secondsRemaining;
    timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "onTick", userInfo: nil, repeats: true)
  }
  
  @IBAction func stopTimer(sender: AnyObject){
    timer.invalidate()
    startButton.hidden = false
    plus5Button.hidden = false
    minus5Button.hidden = false
    stopButton.hidden = true
  }
  
  @IBAction func plus5(sender: AnyObject){
    secondsRemaining += 5 * 60
    updateTime()
  }
  
  @IBAction func minus5(sender: AnyObject){
    let newValue = secondsRemaining - 5*60
    
    if(newValue > 1){
      secondsRemaining = newValue;
    }
    
    updateTime()
  }

  func onTick(){
    let newValue = secondsRemaining - 1
    
    if newValue > -1 {
      secondsRemaining = newValue;
      
      //fot testing show notification every 10 seconds
      if(secondsRemaining % 10 == 0)
      {
        showNotification();
      }
      renderTimer()
    }
    else{
      timer.invalidate()
      showNotification()
    }
  }
  
  func updateTime(){
    let (hours, mins, secs)  = secondsToTime(secondsRemaining)
    timerLabel.text = String(format: "%02d:%02d:%02d", hours, mins, secs)
  }

  func renderTimer(){
    updateTime()
    let percentageLeft: Double = (Double(secondsRemaining)/Double(startSeconds)) * 100.0
    progressBar.setValue(CGFloat(percentageLeft), animateWithDuration: 0.5)
  }
  
  func secondsToTime (seconds : Int) -> (Int, Int, Int) {
     return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
  }
  
  func clearUserStorage(){
    let appDomain = NSBundle.mainBundle().bundleIdentifier!
    NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
  }
  
  //delegate
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = manager.location!
    let lat  = location.coordinate.latitude
    let lng = location.coordinate.longitude
    
    NSLog("lat: %f long: %f", lat, lng)
    //when location updates
  }
  
  //delegate
  func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
    NSLog("Unable to get location")
  }
  
  func showEndAlert(){
    let alertView = UIAlertController(title: "Lone Worker", message: "Timer finished", preferredStyle: UIAlertControllerStyle.Alert)
    alertView.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alertView, animated: true, completion: nil)
  }
  
  func showNotification(){
    let notification = UILocalNotification()
    notification.alertAction = "Go back to lone worker";
    notification.alertBody = "Lone worker timer ended"
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    notification.fireDate = NSDate(timeIntervalSinceNow: 0)
    
    UIApplication.sharedApplication().scheduleLocalNotification(notification)
  }
  
  
}

